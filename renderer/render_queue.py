import pika

class Queue():
    def __init__(self, amqp_uri):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=amqp_uri))
        self.channel = connection.channel()

        self.command_queue_name = 'render_queue'
        self.channel.queue_declare(queue=self.command_queue_name)

    def connect(self, consume_callback):
        self.consume_callback = consume_callback

        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(self.on_request, self.command_queue_name)
        self.channel.start_consuming()

    def on_request(self, ch, method, props, body):
        self.reply_to = props.reply_to
        self.correlation_id = props.correlation_id
        self.delivery_tag = method.delivery_tag

        self.consume_callback(body)
    
    def send_message(self, message):
        print('sending msg....')
        self.channel.basic_publish(exchange='',
                        routing_key=self.reply_to,
                        properties=pika.BasicProperties(correlation_id=self.correlation_id),
                        body=message)
        self.channel.basic_ack(delivery_tag=self.delivery_tag)
