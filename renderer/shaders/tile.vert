#version 410
in vec2 a_position;
in vec2 a_tex_coord;

out vec2 tex_coord;
uniform vec2 u_texture_size;

void main() {
//    vec2 zeroToOne = a_position / u_texture_size;

   // convert from 0->1 to 0->2
//    vec2 zeroToTwo = zeroToOne * 2.0;

   // convert from 0->2 to -1->+1 (clipspace)
//    vec2 clipSpace = zeroToTwo - 1.0;

    // gl_Position = vec4(clipSpace * vec2(1, -1), 0, 1);
    gl_Position = vec4(a_position, 0, 1);
    tex_coord = a_tex_coord;
}

// attribute vec2 a_position;
// attribute vec2 a_texCoord;

// uniform vec2 u_resolution;

// varying vec2 v_texCoord;

// void main() {
   // convert the rectangle from pixels to  0.0 to 1.0

   // pass the texCoord to the fragment shader
   // The GPU will interpolate this value between points.

//    v_texCoord = a_texCoord;
// }