from OpenGL.GL import *

angle = 0.0
radius = 200

def setup_filter():
    # conv_kernel = identity_kernel
    # conv_kernel = blur_kernel
    conv_kernel = emboss_kernel

    global angle
    global radius
    angle += .00005
    radius += .01

    kernel_loc = glGetUniformLocation(program, 'u_kernel')
    angle_loc = glGetUniformLocation(program, 'angle')
    radius_loc = glGetUniformLocation(program, 'radius')

    glUniform1fv(kernel_loc, len(conv_kernel), conv_kernel)
    glUniform1f(angle_loc, angle)
    glUniform1f(radius_loc, radius)