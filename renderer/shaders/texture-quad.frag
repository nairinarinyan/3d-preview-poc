#version 410 core
in vec2 tex_coord;

uniform sampler2D diffuse_texture;
uniform sampler2D meta_texture;
uniform sampler2D uv_texture;

uniform float u_kernel[9];
uniform vec2 u_texture_size;

uniform float radius;
uniform float angle;

out vec4 frag_color;

// apply convolution kernel
vec4 conv(sampler2D tex, vec2 texture_size, vec2 tex_coord, float kernel[9])
{
    vec4 sum = vec4(0.0);
    vec2 step_size = 1.0/texture_size;

    sum += texture(tex, vec2(tex_coord.x - step_size.x, tex_coord.y - step_size.y)) * kernel[0];
    sum += texture(tex, vec2(tex_coord.x, tex_coord.y - step_size.y)) * kernel[1];
    sum += texture(tex, vec2(tex_coord.x + step_size.x, tex_coord.y - step_size.y)) * kernel[2];
    
    sum += texture(tex, vec2(tex_coord.x - step_size.x, tex_coord.y)) * kernel[3];
    sum += texture(tex, vec2(tex_coord.x, tex_coord.y)) * kernel[4];
    sum += texture(tex, vec2(tex_coord.x + step_size.x, tex_coord.y)) * kernel[5];

    sum += texture(tex, vec2(tex_coord.x - step_size.x, tex_coord.y + step_size.y)) * kernel[6];
    sum += texture(tex, vec2(tex_coord.x, tex_coord.y + step_size.y)) * kernel[7];
    sum += texture(tex, vec2(tex_coord.x + step_size.x, tex_coord.y + step_size.y)) * kernel[7];

    sum.a = 1.0;

    return sum;
}
 
// Swirl effect 
vec4 swirl_fx(sampler2D tex, vec2 texture_size, vec2 tex_coord)
{
    vec2 center = texture_size / 2;
    vec2 tc = tex_coord * texture_size;

    tc -= center;
    float dist = length(tc);

    if (dist < radius) 
    {
        float percent = (radius - dist) / radius;
        float theta = percent * percent * angle * 8.0;
        float s = sin(theta);
        float c = cos(theta);
        tc = vec2(dot(tc, vec2(c, -s)), dot(tc, vec2(s, c)));
    }

    tc += center;

    return texture(tex, tc / u_texture_size);
}

void main (void)
{
    // frag_color = conv(uv_texture, u_texture_size, tex_coord, u_kernel);
    frag_color = swirl_fx(uv_texture, u_texture_size, tex_coord);
}
