#version 410
in vec2 tex_coord;

uniform sampler2D u_scene_tex;
uniform sampler2D u_meta_tex;
uniform sampler2D u_pos_tex;
uniform sampler2D u_tile_tex;
uniform float tile_factor;

out vec4 frag_color;

void main() {
    float tile_size = 4096.0;

    // 0:1 -> 0:255
    vec4 rgb = texture(u_pos_tex, tex_coord) * 255.0;

    float color24 = (rgb.r * 256.0 * 256.0) + (rgb.g * 256.0) + rgb.b;

    float y = floor(color24 / tile_size);
    float x = color24 - y * tile_size;

    vec2 v_posCoord = vec2(x/tile_size, y/tile_size) * tile_factor;

    // Look up a pixel from scene texture
    vec4 scene_color = texture(u_scene_tex, tex_coord);

    // Get alpha value from alpha map
    vec4 meta_color = texture(u_meta_tex, tex_coord);

    // used both in opacity and blur
    float opacity_blur = 1.0 - meta_color.b;

    vec4 shine = vec4(meta_color.ggg, 1.0);

    vec4 tile_color = texture( u_tile_tex, v_posCoord );

    // return the 2 colors multiplied, weighted
    vec4 main_color = (scene_color * tile_color * meta_color.g) + (scene_color * opacity_blur);

    frag_color = min(main_color, vec4(1.0));
    // frag_color = (scene_color * (tile_color/41.0) * meta_color.g) + (scene_color * opacity_blur);
}