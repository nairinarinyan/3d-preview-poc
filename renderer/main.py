from OpenGL.GL import *
import numpy as np
import glfw
import importlib
from PIL import Image
from os import environ, path
import sys

from painter import draw_quad
from image import load_image, save_image
from native_window import create_window_context, main_loop
from utils import load_shaders, load_textures
from time import time
import json

from render_queue import Queue

is_offscreen = environ.get('PYOPENGL_PLATFORM') == 'egl'
width = 800
height = 600

if is_offscreen:
    pbuffer = importlib.import_module('pbuffer', '.create_offscreen_context')
    pbuffer.create_offscreen_context(width, height)
    glViewport(0, 0, width, height)
else:
    # window = create_window_context(visible_width, visible_height)
    window = create_window_context(width, height)
    # glViewport(0, 0, visible_width, visible_height)
    # glViewport(0, 0, width, height)

def setup():
    program = load_shaders('tile')
    draw_quad()

    return program

def draw(scale):
    print('drawing....')
    glClear(GL_COLOR_BUFFER_BIT)
    glClearColor(1, 1, 1, 1.0)

    glUseProgram(program)

    handle_framebuffer()

    handle_tiling(scale)

    handle_textures()

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4)

def handle_framebuffer():
    fbo = glGenFramebuffers(1)
    glBindFramebuffer(GL_FRAMEBUFFER, fbo)

    render_buffer = glGenRenderbuffers(1)
    glBindRenderbuffer(GL_RENDERBUFFER, render_buffer)
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB8, width, height)

    print('rendering into framebuffer ', fbo, render_buffer)

    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, render_buffer)

def handle_tiling(scale):
    tile_factor_loc = glGetUniformLocation(program, 'tile_factor')
    glUniform1f(tile_factor_loc, scale)

def handle_textures():
    for idx, texture_data in enumerate(loaded_textures):
        texture, texture_loc = texture_data

        glUniform1i(texture_loc, idx)
        glActiveTexture(getattr(OpenGL.GL, 'GL_TEXTURE{}'.format(idx)))
        glBindTexture(GL_TEXTURE_2D, texture)

    texture_size_loc = glGetUniformLocation(program, 'u_texture_size')
    glUniform2f(texture_size_loc, width, height)


program = setup()

def setup_textures(images_paths):
    global loaded_textures
    global width
    global height

    diffuse_img_data, diffuse_width, diffuse_height = load_image(images_paths['scene'])
    meta_img_data, meta_width, meta_height = load_image(images_paths['metadata'])
    uv_img_data, uv_width, uv_height = load_image(images_paths['uvMapping'])
    tile_img_data, tile_width, tile_height = load_image(images_paths['texture'])

    width = diffuse_width
    height = diffuse_height

    # name, image data, has alpha channel
    textures_to_load = [
        ('u_scene_tex', diffuse_img_data, False, diffuse_width, diffuse_height),
        ('u_meta_tex', meta_img_data, False, meta_width, meta_height),
        ('u_pos_tex', uv_img_data, False, uv_width, uv_height),
        ('u_tile_tex', tile_img_data, True, tile_width, tile_height)
    ]
    loaded_textures = load_textures(program, textures_to_load)

def consume_callback(body):
    data = json.loads(body.decode())
    try:
        start_time = time()
        setup_textures(data)
        print("--- %s --- setup_textures" % (time() - start_time))
    except:
        return queue.send_message('Error invalid file or path.')
    
    glViewport(0, 0, width, height)

    try:
        start_time = time()
        draw(data['scale'])
        print("--- %s --- draw" % (time() - start_time))
    except:
        return queue.send_message('Error occurred during the rendering.')
    
    try:
        start_time = time()
        save_image(width, height, data['output'])
        print("--- %s --- save_image" % (time() - start_time))
    except:
        return queue.send_message('Couldn\'t save the image.')
    
    queue.send_message('OK')


# if is_offscreen:
queue = Queue('localhost')
queue.connect(consume_callback)

# consume_callback('foo')
# else:
#     main_loop(window, draw)
