from OpenGL.GL import *
import numpy as np
import glfw
import importlib
from PIL import Image
from os import environ

from painter import draw_quad
from image import load_image, save_image
from native_window import create_window_context, main_loop
from utils import load_shaders, load_textures

from async_queue import AsyncQueue

is_offscreen = environ.get('PYOPENGL_PLATFORM') == 'egl'

diffuse_img_data, width, height = load_image('./images/scene.jpg')
meta_img_data = load_image('./images/meta.jpg')[0]
uv_img_data = load_image('./images/uv.png')[0]

visible_width = round(width * .8)
visible_height = round(height * .8)

if is_offscreen:
    pbuffer = importlib.import_module('pbuffer', '.create_offscreen_context')
    pbuffer.create_offscreen_context(width, height)
    glViewport(0, 0, width, height)
else:
    window = create_window_context(visible_width, visible_height)
    # window = create_window_context(width, height)
    glViewport(0, 0, visible_width, visible_height)
    # glViewport(0, 0, width, height)

def setup():
    program = load_shaders('tile')
    draw_quad()

    return program

def draw():
    glClear(GL_COLOR_BUFFER_BIT)
    glClearColor(1, 1, 1, 1.0)

    glUseProgram(program)

    handle_textures()

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4)

def handle_textures():
    for idx, texture_data in enumerate(loaded_textures):
        texture, texture_loc = texture_data

        glUniform1i(texture_loc, idx)
        glActiveTexture(getattr(OpenGL.GL, 'GL_TEXTURE{}'.format(idx)))
        glBindTexture(GL_TEXTURE_2D, texture)

    texture_size_loc = glGetUniformLocation(program, 'u_texture_size')
    glUniform2f(texture_size_loc, width, height)


program = setup()

# name, image data, has alpha channel
textures_to_load = [
    ('u_base_tex', diffuse_img_data, False),
    ('u_meta_tex', meta_img_data, False),
    ('u_uv_tex', uv_img_data, False)
]

loaded_textures = load_textures(program, width, height, textures_to_load)

def prepare_tile_texture(texture_bytes):
    pass

def consume_callback(body):
    message = body.decode('ascii')

    tile_img_data = load_image('./images/{}.png'.format(message))[0]
    tile_texture_to_load = [
        ('u_tile_tex', tile_img_data, True)
    ]
    tile_texture = load_textures(program, width, height, tile_texture_to_load)[0]
    loaded_textures.append(tile_texture)

    draw()
    save_image(width, height, 'out.png')


if is_offscreen:
    queue = AsyncQueue('amqp://localhost:5672')
    queue.connect(consume_callback)

else:
    main_loop(window, draw)
