from PIL import Image
from OpenGL.GL import *
import numpy as np
import io
from time import time

# read and save image
def save_image(width, height, images_path):
    buffer = glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE);
    # start_time = time()
    image = Image.frombytes(mode="RGB", size=(width, height), data=buffer)
    image = image.transpose(Image.FLIP_TOP_BOTTOM)
    # print("--- %s 1 ---" % (time() - start_time))
    # print('saving.... ', images_path)
    # start_time = time()
    image.save(images_path, compress_level=1)
    # print("--- %s 2 ---" % (time() - start_time))

def load_image(imagePath):
    start_time = time()
    image = Image.open(imagePath)
    img_data = np.array(image)

    width, height = image.size
    return img_data, width, height

def load_image_buffer(buffer):
    image = Image.open(io.BytesIO(buffer))
    return np.array(list(image.getdata()), np.uint8)