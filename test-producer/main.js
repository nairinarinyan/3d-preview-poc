const amqp = require('amqplib/callback_api');
const fs = require('fs');
const path = require('path');

// const img = './images/baske.png';
const sendTo = 'render_queue';
const replyTo = 'render_queue__back';

// const uvBuffer = fs.readFileSync(img, null).buffer;
// const sendingData = new Buffer(uvBuffer);
const data = {
    texture: path.join(__dirname, './images', 'tile.png'),
    scene: path.join(__dirname, './images', 'scene.jpg'),
    metadata: path.join(__dirname, './images', 'meta.jpg'),
    uvMapping: path.join(__dirname, './images', 'uv.png'),
    scale: 5.8,
    output: path.join(__dirname, './images', 'oops.png')
};

const sendingData = new Buffer(JSON.stringify(data, null, 4));

amqp.connect('amqp://localhost', (err, conn) => {
    // Sending channel
    conn.createChannel((err, ch) => {
        ch.assertQueue(sendTo, { durable: false });
        ch.sendToQueue(sendTo, sendingData, { replyTo, correlationId: '1' });
        console.log(" [x] Sent to %s", sendingData);
    });

    // Receiving channel
    conn.createChannel((err, ch) => {
        ch.assertQueue(replyTo, { durable: false });
        ch.consume(replyTo, msg => {
            // console.log(msg);
            console.log(" [x] Received %s", msg.content.toString());
        }, { noAck: true });
    });
});