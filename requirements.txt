glfw==1.4.0
numpy==1.13.3
pika==0.11.2
Pillow==3.1.2
PyOpenGL==3.1.0
PyOpenGL-accelerate==3.1.0
