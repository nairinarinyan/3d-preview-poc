FROM phusion/baseimage:latest

CMD ["/sbin/my_init"]

ENV PYOPENGL_PLATFORM egl

ENV DISPLAY :0

RUN sed -i "1i deb mirror://mirrors.ubuntu.com/mirrors.txt precise main restricted universe multiverse" /etc/apt/sources.list && \
    apt-get update

RUN apt-get install -y \
    libegl1-mesa-dev libgl1-mesa-dri cmake unzip xvfb \
    python3 python3-pip libjpeg8-dev zlib1g-dev libglfw3-dev

COPY . /home

WORKDIR /home

RUN pip3 install -r requirements.txt

ENTRYPOINT Xvfb :0 -screen 0 800x600x24
